﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GamePreferences
{
    public static string EasyDifficulty = "EasyDifficulty";
    public static string MediumDifficulty = "MediumDifficulty";
    public static string HardDifficulty = "HardDifficulty";

    public static string EasyDifficultyHighScore = "EasyDifficultyHighScore";
    public static string MediumDifficultyHighScore = "MediumDifficultyHighScore";
    public static string HardDifficultyHighScore = "HardDifficultyHighScore";

    public static string EasyDifficultyCoinScore = "EasyDifficultyCoinScore";
    public static string MediumDifficultyCoinScore = "MediumDifficultyCoinScore";
    public static string HardDifficultyCoinScore = "HardDifficultyCoinScore";

    public static string IsMusicOn = "IsMusicOn";

    /*Indications:
     *0 - false, 1 - true
     *0 - off, 1 - on
     */
    //Get and Set Music setting
    //---------------------------------------------------------------
    public static int GetMusicState()
    {
        return PlayerPrefs.GetInt(GamePreferences.IsMusicOn);
    }

    public static void SetMusicState(int state)
    {
        PlayerPrefs.SetInt(GamePreferences.IsMusicOn, state);
    }

    //---------------------------------------------------------------
    //Get and Set Easy difficulty setting
    public static int GetEasyDifficultyState()
    {
        return PlayerPrefs.GetInt(GamePreferences.EasyDifficulty);
    }
    public static void SetEasyDifficultyState(int state)
    {
        PlayerPrefs.SetInt(GamePreferences.EasyDifficulty, state);
    }

    //Get and Set Medium difficulty setting
    public static int GetMediumDifficultyState()
    {
        return PlayerPrefs.GetInt(GamePreferences.MediumDifficulty);
    }
    public static void SetMediumDifficultyState(int state)
    {
        PlayerPrefs.SetInt(GamePreferences.MediumDifficulty, state);
    }

    //Get and Set Hard Difficulty setting
    public static int GetHardDifficultyState()
    {
        return PlayerPrefs.GetInt(GamePreferences.HardDifficulty);
    }
    public static void SetHardDifficultyState(int state)
    {
        PlayerPrefs.SetInt(GamePreferences.HardDifficulty, state);
    }

    //---------------------------------------------------------------
    //Get and Set Easy difficulty Highscore setting
    public static int GetEasyDifficultyHighScore()
    {
        return PlayerPrefs.GetInt(GamePreferences.EasyDifficultyHighScore);
    }
    public static void SetEasyDifficultyHighScore(int highScore)
    {
        PlayerPrefs.SetInt(GamePreferences.EasyDifficultyHighScore, highScore);
    }

    //Get and Set Medium difficulty Highscore setting
    public static int GetMediumDifficultyHighScore()
    {
        return PlayerPrefs.GetInt(GamePreferences.MediumDifficultyHighScore);
    }
    public static void SetMediumDifficultyHighScore(int highScore)
    {
        PlayerPrefs.SetInt(GamePreferences.MediumDifficultyHighScore, highScore);
    }

    //Get and Set Hard difficulty Highscore setting
    public static int GetHardDifficultyHighScore()
    {
        return PlayerPrefs.GetInt(GamePreferences.HardDifficultyHighScore);
    }
    public static void SetHardDifficultyHighScore(int highScore)
    {
        PlayerPrefs.SetInt(GamePreferences.HardDifficultyHighScore, highScore);
    }

    //---------------------------------------------------------------
    //Get and Set Easy difficulty Coinscore setting
    public static int GetEasyDifficultyCoinScore()
    {
        return PlayerPrefs.GetInt(GamePreferences.EasyDifficultyCoinScore);
    }
    public static void SetEasyDifficultyCoinScore(int coinScore)
    {
        PlayerPrefs.SetInt(GamePreferences.EasyDifficultyCoinScore, coinScore);
    }

    //Get and Set Medium difficulty Coinscore setting
    public static int GetMediumDifficultyCoinScore()
    {
        return PlayerPrefs.GetInt(GamePreferences.MediumDifficultyCoinScore);
    }
    public static void SetMediumDifficultyCoinScore(int coinScore)
    {
        PlayerPrefs.SetInt(GamePreferences.MediumDifficultyCoinScore, coinScore);
    }

    //Get and Set Hard difficulty Coinscore setting
    public static int GetHardDifficultyCoinScore()
    {
        return PlayerPrefs.GetInt(GamePreferences.HardDifficultyCoinScore);
    }
    public static void SetHardDifficultyCoinScore(int coinScore)
    {
        PlayerPrefs.SetInt(GamePreferences.HardDifficultyCoinScore, coinScore);
    }

}
