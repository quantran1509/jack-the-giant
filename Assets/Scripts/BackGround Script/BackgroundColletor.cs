﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundColletor : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D target)
    {
        if (target.CompareTag("BG"))
        {
            target.gameObject.SetActive(false);
        }
    }
}
