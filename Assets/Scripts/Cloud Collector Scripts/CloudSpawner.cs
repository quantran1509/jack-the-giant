﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudSpawner : MonoBehaviour {

	[SerializeField]
	private GameObject[] clouds;

	private float distanceBetweenClouds = 3f;

	private float minX = -2.3f, maxX = 2.3f;

	private float lastCloudPosY;

	private float controlX;

	[SerializeField]
	private GameObject[] collectibles;
	[SerializeField]
	private GameObject player;


    private void Awake()
    {
		controlX = 0;
		SetMinAndMaxX();
		CreateClouds();
	}

    // Use this for initialization
    void Start () {

		PositionThePlayer();
	}
	
	void SetMinAndMaxX()
    {
		Vector3 bound = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0));

		maxX = bound.x - 0.5f;
		minX = -bound.x + 0.5f;

    }

	//random cloud position 
	void Shuffle(GameObject[] cloudsToShuffle)
    {
        for (int i = 0; i < cloudsToShuffle.Length; i++)
        {
			GameObject temp = cloudsToShuffle[i];
			int random = Random.Range(i, cloudsToShuffle.Length);
			cloudsToShuffle[i] = cloudsToShuffle[random];
			cloudsToShuffle[random] = temp; 

        }
    }  

	//generate clouds
	void CreateClouds()
    {
		Shuffle(clouds);

		float positionY = 0f;

        for (int i = 0; i < clouds.Length; i++)
        {
			Vector3 temp = clouds[i].transform.position;
			temp.y = positionY;
			temp.x = Random.Range(minX, maxX);

			
            if (controlX == 0)
            {
				temp.x = Random.Range(0.0f, maxX);
				controlX = 1;
            }
			else if (controlX == 1)
			{
				temp.x = Random.Range(0.0f, minX);
				controlX = 2;
			}
			else if (controlX == 2)
			{
				temp.x = Random.Range(1.0f, maxX);
				controlX = 3;
			}
			else if (controlX == 3)
			{
				temp.x = Random.Range(-1.0f, minX);
				controlX = 0;
			}
			 
			lastCloudPosY = positionY;
			clouds[i].transform.position = temp;

			positionY -= distanceBetweenClouds;

		}

    }
	
	void PositionThePlayer()
    {
		GameObject[] darkclouds = GameObject.FindGameObjectsWithTag("Deadly");
		GameObject[] cloudsInGame = GameObject.FindGameObjectsWithTag("Cloud");

        for (int i = 0; i < darkclouds.Length; i++)
        {
            if (darkclouds[i].transform.position.y == 0f)
            {
				Vector3 t = darkclouds[i].transform.position;
				darkclouds[i].transform.position = new Vector3(cloudsInGame[0].transform.position.x,
															   cloudsInGame[0].transform.position.y,
															   cloudsInGame[0].transform.position.z);
				cloudsInGame[0].transform.position = t;

            }
        }
		Vector3 temp = cloudsInGame[0].transform.position;
        for (int i = 1; i < cloudsInGame.Length; i++)
        {
            if (temp.y < cloudsInGame[i].transform.position.y)
            {
				temp = cloudsInGame[i].transform.position;
            }
        }
		player.transform.position = temp + new Vector3(0f, 1f, 0f);
    }

    private void OnTriggerEnter2D(Collider2D target)
    {
        if (target.CompareTag("Cloud") || target.CompareTag("Deadly"))
        {
            if (target.transform.position.y == lastCloudPosY)
            {
				Shuffle(clouds);

				Vector3 temp = target.transform.position;
                for (int i = 0; i < clouds.Length; i++)
                {
                    if (!clouds[i].activeInHierarchy)
                    {
						if (controlX == 0)
						{
							temp.x = Random.Range(0.0f, maxX);
							controlX = 1;
						}
						else if (controlX == 1)
						{
							temp.x = Random.Range(0.0f, minX);
							controlX = 2;
						}
						else if (controlX == 2)
						{
							temp.x = Random.Range(1.0f, maxX);
							controlX = 3;
						}
						else if (controlX == 3)
						{
							temp.x = Random.Range(-1.0f, minX);
							controlX = 0;
						}

						temp.y -= distanceBetweenClouds;

						lastCloudPosY = temp.y;

						clouds[i].transform.position = temp;
						clouds[i].SetActive(true);

						//Spawn Collectables

						int random = Random.Range(0, collectibles.Length);

                        if (!clouds[i].CompareTag("Deadly"))
                        {
                            if (!collectibles[random].activeInHierarchy)
                            {
								Vector3 temp2 = clouds[i].transform.position;
								temp2.y += 0.7f;

                                if (collectibles[random].CompareTag("Life"))
                                {
                                    if (PlayerScore.lifeCount < 2)
                                    {
										collectibles[random].transform.position = temp2;

										collectibles[random].SetActive(true);
									}
                                }
								else
								{
									collectibles[random].transform.position = temp2;

									collectibles[random].SetActive(true);
								}
							}
                        }
					}
                }
				
            }
        }
    }
















}
