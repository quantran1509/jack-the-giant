﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class PlayerScore : MonoBehaviour
{
    [SerializeField] private AudioClip coinClip, lifeClip;

    public static int coinCount;
    public static int lifeCount;
    public static int scoreCount;

    private Vector3 previousPosition;
    private CameraScript cameraScript;

    private bool countScore;
    private void Awake()
    {
        cameraScript = Camera.main.GetComponent<CameraScript>();
    }
    // Start is called before the first frame update
    void Start()
    {
        previousPosition = transform.position;
        countScore = true;
    }

    // Update is called once per frame
    void Update()
    {
        CountScore();
    }

    void CountScore()
    {
        if (countScore)
        {
            if (transform.position.y < previousPosition.y)
            {
                scoreCount++;
                GameplayController.instance.SetScore(scoreCount);
            }
            previousPosition = transform.position;
        }
       
    }
    private void OnTriggerEnter2D(Collider2D target)
    {
        if (target.CompareTag("Coin"))
        {
            coinCount++;
            scoreCount += 200;

            AudioSource.PlayClipAtPoint(coinClip, transform.position);
            target.gameObject.SetActive(false);

            GameplayController.instance.SetCoinScore(coinCount);
            GameplayController.instance.SetScore(scoreCount);
        }
        if (target.CompareTag("Life"))
        {
            lifeCount++;
            scoreCount += 200;
            AudioSource.PlayClipAtPoint(lifeClip, transform.position);
            target.gameObject.SetActive(false);

            GameplayController.instance.SetLifeScore(lifeCount);
            GameplayController.instance.SetScore(scoreCount);
        }
        if (target.CompareTag("Deadly") || target.CompareTag("Bound"))
        {
            cameraScript.moveCamera = false;
            transform.position = new Vector3(500f, 500f, 0);
            countScore = false;
            lifeCount--;

            GameManager.instance.CheckGameStatus(scoreCount, coinCount, lifeCount);

        }
        

    }
}
