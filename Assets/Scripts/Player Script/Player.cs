﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public float speed = 8f, maxVelocity = 4f;

    private Rigidbody2D rb;
    private Animator anim;

    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        PlayerMove();

    }
    

    void PlayerMove()
    {
        float forceX = 0f;
        float vel = Mathf.Abs(rb.velocity.x);

        float moveDir = Input.GetAxisRaw("Horizontal");

        if (moveDir > 0)
        {
            if (vel < maxVelocity)
            {
                forceX = speed;
            }

            //Play walk anim
            anim.SetBool("Walk", true);
            Debug.Log("Walked");
            //Flip player
            Vector3 temp = transform.localScale;
            temp.x = 1.3f;
            transform.localScale = temp;

        }

        else if (moveDir < 0)
        {
            if (vel < maxVelocity)
            {
                forceX = -speed;
            }
            //Play walk anim
            anim.SetBool("Walk", true);

            //Flip player
            Vector3 temp = transform.localScale;
            temp.x = -1.3f;
            transform.localScale = temp;
        }
        else
        {
            //Stop playing walk anim
            anim.SetBool("Walk", false);
        }

        rb.AddForce(new Vector2(forceX, 0));
    }



} //Player
