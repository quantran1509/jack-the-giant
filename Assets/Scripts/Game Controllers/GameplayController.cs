﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameplayController : MonoBehaviour
{
    [SerializeField] private Text scoreText, coinCountText, lifeCountText, gameOverScoreText, gameOverCoinText;
    [SerializeField] private GameObject pausePanel, gameOverPanel;

    public static GameplayController instance;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
 
    private void Start()
    {
        pausePanel.SetActive(false);
    }

    public void GameOverShowPanel(int finalScore, int finalCoinScore)
    {
        gameOverPanel.SetActive(true);
        gameOverScoreText.text = finalScore.ToString();
        gameOverCoinText.text = finalCoinScore.ToString();
        Invoke("GameOverLoadMainMenu", 3f);
    }
    public void PlayerDiedRestartTheGame()
    {
        Invoke("PlayerDiedRestart", 1f);
    }
    void PlayerDiedRestart()
    {
        SceneManager.LoadScene("GamePlay");
    }
    void GameOverLoadMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
    public void SetScore(int score)
    {
        scoreText.text = "x " + score;
    }
    public void SetLifeScore(int lifeScore)
    {
        lifeCountText.text = "x " + lifeScore;
    }
    public void SetCoinScore(int coinScore)
    {
        coinCountText.text = "x " + coinScore;
    }

    public void PauseTheGame()
    {
        Time.timeScale = 0f;
        pausePanel.gameObject.SetActive(true);
    }
    public void ResumeTheGame()
    {
        Time.timeScale = 1f;
        pausePanel.gameObject.SetActive(false);
    }
    public void QuitTheGame()
    {
        Time.timeScale = 1f;
        pausePanel.gameObject.SetActive(false);
        SceneManager.LoadScene("MainMenu");
    }

}
