﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableScipts : MonoBehaviour
{
    private void OnEnable()
    {
        Invoke("DeactivateCollectables", 7f);
    }
    void DeactivateCollectables()
    {
        gameObject.SetActive(false);
    }
}
